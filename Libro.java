import java.util.*;
public class Libro{
	private String titulo;
	private int nPag;
	private String autor;
	private String editorial;
	private int anioImp;
	private int anioDist;
	private String isbn;
	static int numero;
	
	public Libro(){
		numero++;
	}
	
	public Libro(String titulo,String autor){
		this.titulo=titulo;
		this.autor=autor;
		numero++;
	}
	
	public Libro(String titulo,int nPag,String autor,String editorial,int anioImp,int anioDist,String isbn){
		this.titulo=titulo;
		this.nPag=nPag;
		this.autor=autor;
		this.editorial=editorial;
		this.anioImp=anioImp;
		this.anioDist=anioDist;
		this.isbn=isbn;
	}
	
	public void imprime(){
		System.out.println("Titulo Libro: "+titulo+" "+"Numero paginas: "+nPag+" "+"Nombre Autor: "+autor);
	}
	
	public void setTitulo (String titulo){
		this.titulo=titulo;
	}
	
	public void setNpag (int nPag){
		this.nPag=nPag;
	}
	
	public void setAutor (String autor){
		this.autor=autor;
	}
	
	public void setEditorial (String editorial){
		this.editorial=editorial;
	}
	
	public void setAnioImp (int anioImp){
		this.anioImp=anioImp;
	}
	
	public void setAnioDist (int anioDist){
		this.anioDist=anioDist;
	}
	
	public void setIsbn (String isbn){
		this.isbn=isbn;
	}
	
	public String getTitulo (){
		return titulo;
	}
	
	public int getNpag (){
		return nPag;
	}
	
	public String getAutor (){
		return autor;
	}
	
	public String getEditorial (){
		return editorial;
	}
	
	public int getAnioImp (){
		return anioImp;
	}
	
	public int getAnioDist (){
		return anioDist;
	}
	
	public String getIsbn (){
		return isbn;
	}
}
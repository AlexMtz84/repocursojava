import java.util.*;
public class Persona{
	private String nombre;
	private String aPaterno;
	private String aMaterno;
	private int edad;
	
	public void imprime(){
		if (nombre==null){
			System.out.println("La persona no tiene nombre!"); 
		}
		else{
			if (aPaterno==null||aMaterno==null)
				System.out.println("Soy ["+nombre+"] pero no tengo apellidos!");
			else
				System.out.println("Me llamo "+nombre+" "+aPaterno+" "+aMaterno+"\nTengo ["+edad+"] años de edad");
		}	
	}
	
	public Persona(){
	}
	
	public Persona(String nombre){
		this.nombre=nombre;
	}
	
	public Persona(String nombre, int edad){
		this(nombre);   //para asignar el nombre hacemos uso del constructor de arriba 
		this.edad=edad;
	}
	
	public Persona(String nombre,String aPaterno,String aMaterno, int edad){
		this(nombre,edad);
		this.aPaterno=aPaterno;
		this.aMaterno=aMaterno;
	}
	
	public void setNombre (String nombre){
		this.nombre=nombre;
	}
	
	public void setApaterno (String aPaterno){
		this.aPaterno=aPaterno;
	}
	
	public void setAmaterno (String aMaterno){
		this.aMaterno=aMaterno;
	}
	
	public void setEdad (int edad){
		this.edad=edad;
	}
	
	public String getNombre (){
		return nombre;
	}
	
	public String getApaterno (){
		return aPaterno;
	}
	
	public String getAmaterno (){
		return aMaterno;
	}
	
	public int getEdad (){
		return edad;
	}
}
import java.util.*;
public class Usuario extends Persona{
	private int id;
	private String clave;
	private String calle;
	private String colonia;
	private String ciudad;
	
	public Usuario(String nombre,String aPaterno,String aMaterno,int edad,int id,String clave,String calle,String colonia,String ciudad){
		super (nombre,aPaterno,aMaterno,edad);
		this.id=id;
		this.clave=clave;
		this.calle=calle;
		this.colonia=colonia;
		this.ciudad=ciudad;
		
	}

	public void setId (int id){
		this.id=id;
	}
	
	public void setClave (String clave){
		this.clave=clave;
	}
	
	public void setCalle (String calle){
		this.calle=calle;
	}
	
	public void setColonia (String colonia){
		this.colonia=colonia;
	}
	
	public void setCiudad (String ciudad){
		this.ciudad=ciudad;
	}
	
	public int getId (){
		return id;
	}
	
	public String getClave (){
		return clave;
	}
	
	public String getCalle (){
		return calle;
	}
	
	public String getColonia (){
		return colonia;
	}
	
	public String getCiudad (){
		return ciudad;
	}
}
import java.util.*;
public class Autor extends Persona{
	private int numLibros;
	private String nombreArtistico;
	private int maxPaginasEscritas;
	private int aniosExp;
	
	//constructores
	public Autor(String nombre){
		super(nombre);
	}
	
	//seccion set
	public void setNumLibros(int numLibros){
		this.numLibros=numLibros;
	}
	
	public void setNomArt(String nombreArtistico){
		this.nombreArtistico=nombreArtistico;
	}
	
	public void setMaxPag(int maxPaginasEscritas){
		this.maxPaginasEscritas=maxPaginasEscritas;
	}
	
	public void setAniosExp(int aniosExp){
		this.aniosExp=aniosExp;
	}
	
	//seccion get
	public int getNumLibros(){
		return numLibros;
	}
	
	public String getNomArt(){
		return nombreArtistico;
	}
	
	public int getMaxPag(){
		return maxPaginasEscritas;
	}
	
	public int getAniosExp(){
		return aniosExp;
	}
	
	//metodos
	public void imprime(){
		super.imprime();
		System.out.println("He escrito "+numLibros+" libros");
	}
	
}